jest.mock('aws-lambda-ses-forwarder');

const LambdaForwarder = require('aws-lambda-ses-forwarder');
const handler = require('./index').handler;

describe('nextport-emailer', () => {
  const expectedFromAddress = 'example@sender.com';
  const expectedBucket = 's3emailbucket';
  const expectedKeyPrefix = 'emails/'
  const expectedForwardMapping = { 'example@incoming.com': ['example@forwardaddr.com']};
  const fakeEvent = { eventType: 'Test'};
  const fakeContext = { functionName: 'nextport-emailer' };
  const fakeCallback = () => {};

  beforeEach(() => {
    // Assign
    process.env.fromEmail = expectedFromAddress;
    process.env.emailBucket = expectedBucket;
    process.env.emailKeyPrefix = expectedKeyPrefix; 
    process.env.forwardMapping = JSON.stringify(expectedForwardMapping);

    jest.resetAllMocks();
  });

  it('extracts settings from the environement and forwards them to aws-lambda-ses-forwarder', () => {
    // Act
    handler(fakeEvent, fakeContext, fakeCallback);

    // Assert
    expect(LambdaForwarder.handler).toHaveBeenCalledWith(fakeEvent, fakeContext, fakeCallback, {
      config: {
        fromEmail: expectedFromAddress,
        emailBucket: expectedBucket,
        emailKeyPrefix: expectedKeyPrefix,
        forwardMapping: expectedForwardMapping
      }
    });
  });

  it('gracefully handles missing settings from the environement and forwards them to aws-lambda-ses-forwarder', () => {
    // Assign
    process.env.forwardMapping = '{"missing@closing:["quote@mark"]}';
    jest.spyOn(console, 'error');

    // Act
    handler(fakeEvent, fakeContext, fakeCallback);

    // Assert
    expect(console.error).toHaveBeenCalled();
    expect(console.error.mock.calls[0][0].indexOf('nextport-emailer: The provided forward mapping wasn\'t valid JSON: ')).toBe(0);
    expect(LambdaForwarder.handler).not.toHaveBeenCalled();
  });
});