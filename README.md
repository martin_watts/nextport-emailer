# NextPort Emailer
A companion package to NextPort; It simplifies the steps to get SES email forwarding working via an AWS Lambda. 

## Credits
All the heavy lifting is done by [aws-lambda-ses-forwarder](https://github.com/arithmetric/aws-lambda-ses-forwarder) and [serverless](https://serverless.com).

## Getting started
These instructions will get you a copy of the project on your local machine and prepare you for deployment to AWS. 

### Prerequisites
- The project is written using modern ES6 JavaScript, and is developed to target Node.js 8+.
  I recommended using the latest LTS version of [Node.js](https://nodejs.org/en/download/) for your OS.

- The project prefers [yarn](https://yarnpkg.com/) over npm.

- This project relies on you being able to send and recieve emails via SES. 
  This assumes you have already completed the steps to make this possible.

- Follow AWS instructions to:
  - [Verify a domain with SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domains.html)
  - [Publish an MX record to forward email to SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/receiving-email-mx-record.html)
  - [Verify your sender email address](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses.html)

- To take full advantage of the contact form feature, you'll need to register for [ReCAPTCHA](https://www.google.com/recaptcha/) and generate site keys:

The rest of this guide assumes you've followed the above steps.


### Installing
1) Clone the repository

`git clone https://gitlab.com/martin_watts/nextport-emailer.git/`

2) Run a yarn install in the root of the repository

`cd nextport-emailer`

`yarn install`


3) Install serverless and the aws-cli globally

`yarn global add serverless aws-cli`

4) Check that you can run serverless

`serverless version` 

If you see a 'command not found' error, the global yarn package directory is probably not in your PATH 
environment variable. You can find this location by running:

`yarn global bin`

How you update your PATH variable varies depending on your OS. 
On Ubuntu 18.10, I added `export PATH="$(yarn global bin):$PATH"` to the end of my ~/.bashrc


5) [Configure aws-cli with your credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)


## Development
I really suggest you've read and understood the contents of serverless.yml before attempting a deployment. 
This project makes use of Serverless, which in turn makes use of AWS CloudFormation, to set up resources 
in your AWS account.

These resources are:

- S3 Buckets: One to store the deployment artefacts, one to store emails received via SES.
- An S3 Bucket Policy: To _allow_ SES to store emails in the appropriate S3 bucket.
- An SES Receipt Rule/RuleSet: When made active, these cause SES to save incoming emails to the S3 bucket 
  and to trigger the forwarder Lambda function
- An IAM Role: Which allows the Lambda function to access the S3 email bucket, log to CloudWatch and send emails via SES
- A Lambda Function: Which does the actual email forwarding, when triggered. SES is given permission to execute the lambda.

This project is pretty opinionated on how these things should be configured so the serverless.yml 
aims for sensible defaults, but might cause you problems, depending on how your AWS account is 
currently configured, but if it fails to deploy, it'll roll back and not leave things in a broken state.

That said, I refer you to the [license](./LICENSE.md); if you run this code you're acknowledging that you 
understand the implications of doing so, and are accepting any risks that may entail.


### Got it, just tell me what I need to configure
Top to bottom, things you'll _need_ to change:

1) The enviroment variables 
```
environment:
  fromEmail: 'replace@me'
  forwardMapping: >
    {
      "incoming@address1": [
        "forwardAddress@1",
        "forwardAddress@2"
      ]
    }
```

The `fromEmail` address should be a verified email address, forwarded emails will _come_ from this address, 
with a replyTo field of the original sender.

The `forwardMapping` object dictates which email addresses will receive a forwarded message, 
based on the address of the original sender. 
This object is stored as a JSON string, and parsed in the lambda function.

From the original docs: 
> Object where the key is the lowercase email address from
> which to forward and the value is an array of email addresses to which to
> send the message.
>
> To match all email addresses on a domain, use a key without the name part
> of an email address before the "at" symbol (i.e. `@example.com`).
>
> To match a mailbox name on all domains, use a key without the "at" symbol
> and domain part of an email address (i.e. `info`).

Note: If your SES account is in sandbox mode, the forward recipients will also need to be verified email addresses. 

2) The recipients in the SES ReceiptRule
```
Type: AWS::SES::ReceiptRule
  Properties:
    Rule:
      ScanEnabled: true
      Recipients: 
        - replace@me
```

The SES rule that saves the email to S3 and triggers the Lambda is set to run only for particular recipients.
The list should be those addresses you wish to receive email on. 
Again, from the [docs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ses-receiptrule-rule.html)
> The recipient domains and email addresses that the receipt rule applies to. 
> If this field is not specified, this rule will match all recipients under all verified domains. 

### Optional configuration
1) The `region` is ` eu-west-1` by default. One small caveat: It needs to be a region with SES and Lambda available, which is currently limited to:
- us-east-1
- us-west-2
- eu-west-1

2) The cleanup lifecycle on the email bucket, by default 7 days, but may be too long/short depending on your requirements:
```
LifecycleConfiguration:
  Rules:
  - ExpirationInDays: '7'
    Id: Cleanup
    Status: Enabled
```

3) The TlsPolicy, set to optional by default, but can be set to Require to enfore all emails having to be sent to SES via a TLS protected connection.
```
TlsPolicy: Optional
```


## Deployment
1) When you've made the requisite changes to serverless.yml, run:

`yarn deploy`

2) Assuming deployment is successful, you will need to set the created ruleset as active:

(Assuming the `region` was still set to `eu-west-1`, otherwise substitue the correct region) 

- Open https://eu-west-1.console.aws.amazon.com/ses/home?region=eu-west-1#receipt-rules:
- Select 'NextPortEmailerReceiptRule' and set it as the active rule setup


## Testing/Troubleshooting
Send an email to someone on your recipient list, it should be routed via SES -> Lambda -> SES -> forward email address. 
If the forwarded message doesn't turn up, time to do some extra debugging. 
You can see from the AWS console whether or not the Lambda is being invoked:

Again, substitue your region:
- Open https://eu-west-1.console.aws.amazon.com/lambda/home?region=eu-west-1#/functions/nextport-emailer-production-forwardemail?tab=monitoring
- Check that the Invocations graph is showing activity, if not the problem is upstream, check your SES settings to ensure the ruleset is active.
- If there's activity but the email isn't getting through there's two main possibilities:
  1) There's an error in the function, check the logs at https://eu-west-1.console.aws.amazon.com/cloudwatch/home?region=eu-west-1#logStream:group=/aws/lambda/nextport-emailer-production-forwardemail;streamFilter=typeLogStreamPrefix
  2) The email address you're sending to has been suppressed by SES. If you suspect this is the case, you can request a suppression removal at https://eu-west-1.console.aws.amazon.com/ses/home?region=eu-west-1#suppressionlist-removal:


If the lambda fails, error logs will be written to CloudWatch logs, but you may wish to set up a CloudWatch alarm for lambda errors, 
so you'll be notified by email as well.

## License
Licensed under the MIT license, see [LICENSE](./LICENSE.md) for details.


## About
Author: [Martin Watts](https://gitlab.com/martin_watts)

&copy; Martin Watts 2019
